import Service from './index.js'
export default class MapService {
  static getCountData (data) {
    return new Promise((resolve, reject) => {
      Service.TEMPLATE_GET('/api/stat/area_count', data, resolve)
    })
  }

  static getMapData (data) {
    return new Promise((resolve, reject) => {
      Service.TEMPLATE_GET('/api/stat/coord', data, resolve)
    })
  }

  static getLLData (data) {
    return new Promise((resolve, reject) => {
      Service.TEMPLATE_GET('/api/stat/traffic', data, resolve)
    })
  }
}

const gis = {
  wh_gis_init: (data, type) => {
    const crs = new L.Proj.CRS("EPSG:4326", "+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs",
      {
        origin: [-180.0, 90.0],
        resolutions: [
          0.703125,
          0.3515625,
          0.17578125,
          0.087890625,
          0.0439453125,
          0.02197265625,
          0.010986328125,
          0.0054931640625,
          0.00274658203125,
          0.001373291015625,
          6.866455078125E-4,
          3.4332275390625E-4,
          1.71661376953125E-4,
          8.58306884765625E-5,
          4.291534423828125E-5,
          2.1457672119140625E-5,
          1.07288360595703125E-5,
          5.36441802978515625E-6,
          2.682209014892578125E-6,
          1.3411045074462890625E-6,
          6.7055225372314453125E-7
        ]
      });
    const url = (() => {
      switch (type) {
        case 1:
          return 'http://10.137.8.2:7001/PGIS_S_TileMapServer/Maps/PGIS25W/EzMap?Service=getImage&Type=RGB&ZoomOffset=1&Col={x}&Row={y}&Zoom={z}&V=1.0.0'
        case 2:
          return 'http://10.137.8.2:7001/PGIS_S_TileMapServer/Maps/ZKYTNCSL/EzMap?Service=getImage&Type=RGB&ZoomOffset=1&Col={x}&Row={y}&Zoom={z}&V=1.0.0'
      }
    })()

    const map = L.map('map', {
      crs: crs,
      center: [28.628476, 115.830482],
      zoom: 8,
      minZoom: 7,
      maxZoom: 18,
      forceDrawEverything: true
    });

    L.tileLayer(url).addTo(map);

    const mapJson = data.map(item => {
      return {
        lng: Number(item[2]),
        lat: Number(item[3]),
        count: Number(item[1])
      }
    })

    const mapv = new window.Mapv({
      map: map,
      useLeaflet: true
    })

    new window.Mapv.Layer({
      mapv: mapv, // 对应的mapv实例
      zIndex: 1, // 图层层级
      dataType: 'point', // 数据类型，点类型
      data: mapJson, // 数据
      dataRangeControl: false, // 值阈控件
      drawType: 'heatmap', // 展示形式
      drawOptions: { // 绘制参数
        maxOpacity: 0.8, // 最大透明度，默认为0.8
        type: 'radius',  // 可选参数有rect(方形)和radius(圆形，默认)
        max: 100000, // 设置最大的权重值
        shadowBlur: 15, // 默认15
        gradient: { // 渐变颜色值设置
          '0.4': 'blue',
          '0.6': 'cyan',
          '0.7': 'lime',
          '0.8': 'yellow',
          '1.0': 'red'
        }
      }
    })
  }
}



export default gis

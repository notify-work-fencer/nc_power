import Vue from 'vue'
import Router from 'vue-router'
import global from '@/components/main/global'
import qs from '@/components/main/qs'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'global',
      component: global
    },
    {
      path: '/qs',
      name: 'qs',
      component: qs
    }
  ]
})

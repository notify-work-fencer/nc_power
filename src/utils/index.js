export default class util {
  constructor () {
    this.hasOwnProperty = Object.prototype.hasOwnProperty
    this.propIsEnumerable = Object.prototype.propertyIsEnumerable
  }

  isObj(x){ 
    const type = typeof x
    return x !== null && (type === 'object' || type === 'function')
  }
  
  toObject(val) {
    if (val === null || val === undefined) {
      throw new TypeError('Cannot convert undefined or null to object')
    }
    return Object(val)
  }
  
  assignKey(to, from, key) {
    const val = from[key]
  
    if (val === undefined || val === null) {
      return
    }
    if (this.hasOwnProperty.call(to, key)) {
      if (to[key] === undefined || to[key] === null) {
        throw new TypeError('Cannot convert undefined or null to object (' + key + ')')
      }
    }
    if (!this.hasOwnProperty.call(to, key) || !this.isObj(val)) {
      to[key] = val
    } else {
      to[key] = this.assign(Object(to[key]), from[key])
    }
  }
  
  assign(to, from) {
    if (to === from) {
      return to
    }
    from = Object(from)
    for (let key in from) {
      if (this.hasOwnProperty.call(from, key)) {
        this.assignKey(to, from, key)
      }
    }
    if (Object.getOwnPropertySymbols) {
      const symbols = Object.getOwnPropertySymbols(from)

      symbols.forEach(item => {
        if (this.propIsEnumerable.call(from, item)) {
          this.assignKey(to, from, item)
        }
      })
    }
    return to
  }
  
  deepAssign(target) {
    target = this.toObject(target)
    
    for (let index in arguments) {
      this.assign(target, arguments[index])
    }
    return target
  };

}
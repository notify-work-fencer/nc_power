// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import ElementUI from 'element-ui'
import Options from '@/utils/bar_option.js'
// import h337 from 'heatmap.js'

const echarts = require('echarts')

import '@/assets/sass/main.scss'
import 'element-ui/lib/theme-chalk/index.css'



Vue.use(ElementUI)

Vue.config.productionTip = false
Vue.prototype.echarts = echarts
Vue.prototype.Options = Options
// Vue.prototype.heatmap = h337

const services = require.context('./service/', true, /\.js$/)
services.keys().forEach(item => {
  if (item !== './index.js') {
    const tmpKey = item.split('/')[item.split('/').length - 1].replace(/_service.js/g, 'Service')
    Vue.prototype[tmpKey] = services(item).default
  }
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
